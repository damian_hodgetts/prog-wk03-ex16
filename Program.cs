﻿using System;

namespace prog_wk03_ex16
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = "";

            Console.WriteLine("*******************************");
            Console.WriteLine("****   Welcome to my app   ****");
            Console.WriteLine("*******************************");
            Console.WriteLine("");
            
            Console.WriteLine("What is your Name? ");
            name = Console.ReadLine();

            Console.WriteLine("");

            Console.WriteLine($"Your name is {name}");

        }
    }
}
